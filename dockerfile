# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/cardnumber-sent.jar /cardnumber-sent.jar
# run application with this command line[
CMD ["java", "-jar", "/cardnumber-sent.jar"]
